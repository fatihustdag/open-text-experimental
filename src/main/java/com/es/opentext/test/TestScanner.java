package com.es.opentext.test;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestScanner {
	private static Client client;


	public static void main(String[] args) throws IOException {
        client = ClientBuilder.newBuilder().register(MultiPartFeature.class).register(JacksonFeature.class).build();

        String sessionId = createSession();

		HashMap<String, Object> loadScannerResult = loadScanner(sessionId);
		System.out.println(loadScannerResult);

        String tokenIdLoad = (String) loadScannerResult.get("TokenID");
        int loadScannerExitCode = waitForTask(sessionId, tokenIdLoad);
        System.out.println("Load Scanner: " + loadScannerExitCode);

        getScannerSettings(sessionId);

        HashMap<String, Object> showScannerResult = showScannerUI(sessionId);
        tokenIdLoad = (String) showScannerResult.get("TokenID");
        waitForTask(sessionId, tokenIdLoad);

        String scanJobToken = createScanJob(sessionId);
        System.out.println("scanJob token: " + scanJobToken);

        int imageCount = waitForScanJob(sessionId, scanJobToken);
        System.out.println("scanJob image count: " + imageCount);

        processScanJob(sessionId, scanJobToken, imageCount);

        int statusCode = endScanJob(scanJobToken);
        System.out.println("endScanJob status code: " + statusCode);

        statusCode = unloadScanner(sessionId);
        System.out.println("unloadScanner status code: " + statusCode);

        statusCode = endSession(sessionId);
        System.out.println("endSession status code: " + statusCode);

	}

	private static void processScanJob(String sessionId, String tokenId, int numberOfImages) throws IOException {
		for(int imgNum = 0; imgNum < numberOfImages; imgNum++) {
			Map<String, Object> body = new HashMap<>();
			Map<String, Object> imgOutput = new HashMap<>();
			imgOutput.put("FileType", 0);
			imgOutput.put("Compression", 0);
			body.put("ImageOutput", imgOutput);
			body.put("Filters", new ArrayList<>());

			Response resp = client.target("http://127.0.0.1:49732/scanservice/v2")
					.path(sessionId)
					.path(tokenId)
					.path(Integer.toString(imgNum))
					.request(MediaType.APPLICATION_JSON)
					.post(Entity.entity(body, MediaType.APPLICATION_JSON));

			HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
			System.out.println("Response for image number " + imgNum + ":\n" + respMap);

			String imageId = (String) respMap.get("ImageID");
			if(imageId != null)
				readImage(sessionId, tokenId, imageId);
		}
	}

	private static void readImage(String sessionId, String tokenId, String imageId) throws IOException {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2")
				.path(sessionId)
				.path(tokenId)
				.path(imageId)
				.queryParam("fileType", 0)
				.queryParam("compression", 0)
				.queryParam("width", 0)
				.queryParam("height", 0)
				.request(MediaType.APPLICATION_JSON)
				.get();
		int status = resp.getStatus();
		if(status == 200) {
			InputStream stream = resp.readEntity(InputStream.class);
			BufferedImage img = ImageIO.read(stream);
			JFrame f = new JFrame();
			f.add(new JLabel(new ImageIcon(img)), BorderLayout.CENTER);
			f.setBounds(0, 0, 600, 800);
			f.setVisible(true);
			ImageIO.write(img, "png", new File("c:/temp/smartcap"+ imageId + ".png"));
			stream.close();
		}
	}

	private static HashMap<String, Object> loadScanner(String sessionId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/loadscanner")
				.queryParam("session", sessionId)
				.queryParam("loadscanneroption", 2)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		return respMap;
	}

	private static int unloadScanner(String sessionId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/unloadscanner")
				.queryParam("session", sessionId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		System.out.println(">> " + respMap);
		int statusCode = (Integer) respMap.get("Status");
		return statusCode;
	}

	private static int getScannerSettings(String sessionId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/getscannersettings")
				.queryParam("session", sessionId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		System.out.println(">> " + respMap);
		int statusCode = (Integer) respMap.get("Status");
		return statusCode;
	}

	private static HashMap<String, Object> showScannerUI(String sessionId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/showscannerui")
				.queryParam("session", sessionId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		System.out.println(">> " + respMap);
		return respMap;
	}

	private static int waitForTask(String sessionId, String tokenId) {
		System.out.println("waitForTask(" + sessionId + ", " + tokenId + ")");

		int statusCode = 0;
		while(statusCode == 0) {
			try { Thread.sleep(500); } catch (InterruptedException e) {}

			System.out.println("Calling getStatus for token " + tokenId);

			Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/getstatus")
					.queryParam("session", sessionId)
					.queryParam("tokenid", tokenId)
					.request(MediaType.APPLICATION_JSON)
					.get();
			HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
			System.out.println(">> " + respMap);
			statusCode = (Integer) respMap.get("StatusCode");
		}
		System.out.println("Exit code for token " + tokenId + ": " + statusCode);
		return statusCode;
	}

	private static int waitForScanJob(String sessionId, String tokenId) {
		System.out.println("waitForTask(" + sessionId + ", " + tokenId + ")");

		int statusCode = 0;
		int numberOfImages = 0;
		while(statusCode == 0) {
			try { Thread.sleep(500); } catch (InterruptedException e) {}

			System.out.println("Calling getStatus for token " + tokenId);

			Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/getstatus")
					.queryParam("session", sessionId)
					.queryParam("tokenid", tokenId)
					.request(MediaType.APPLICATION_JSON)
					.get();
			HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
			System.out.println(">> " + respMap);
			statusCode = (int) respMap.get("StatusCode");
			numberOfImages = (int) respMap.get("NumberOfScannedImages");
		}
		System.out.println("Exit code for token " + tokenId + ": " + statusCode + ", images = " + numberOfImages);
		return numberOfImages;
	}

	private static String createScanJob(String sessionId) {
		Response respCreateScanJob = client.target("http://127.0.0.1:49732/scanservice/v2/createscanjob")
        	.queryParam("session", sessionId)
        	.queryParam("pages", 0)
        	.queryParam("fileType", 1245184)
        	.queryParam("compression", 0)
        	.request(MediaType.APPLICATION_JSON)
        	.get();
        HashMap<String, Object> respScanJob = respCreateScanJob.readEntity(new GenericType<HashMap<String, Object>>() { });
        int sc = (int) respScanJob.get("StatusCode");
        if(sc != 0)
        	throw new RuntimeException("createscanjob failed " + respScanJob);

		return (String) respScanJob.get("TokenID");
	}

	@SuppressWarnings("unused")
    private static int stopScanning(String scanjobId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/stopscanning")
				.queryParam("scanjob", scanjobId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		System.out.println(">> " + respMap);
		int statusCode = (Integer) respMap.get("Status");
		return statusCode;
	}

	private static int endScanJob(String scanjobId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/endscanjob")
				.queryParam("scanjob", scanjobId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		System.out.println(">> " + respMap);
		int statusCode = (Integer) respMap.get("Status");
		return statusCode;
	}

	private static String createSession() {
		WebTarget target = client.target("http://127.0.0.1:49732/scanservice/v2/createsession?license=Some_license&app=SampleApplication&closeexisting=true&locale=en-us&timeout=0&warndisableoption=0");
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        System.out.println("Resp-Status: " + response.getStatus());
        HashMap<String, Object> respEntity = response.readEntity(new GenericType<HashMap<String, Object>>() { });
        String sessionId = (String) respEntity.get("SessionID");
		return sessionId;
	}

	private static int endSession(String sessionId) {
		Response resp = client.target("http://127.0.0.1:49732/scanservice/v2/endsession")
				.queryParam("session", sessionId)
				.request(MediaType.APPLICATION_JSON)
				.get();
		HashMap<String, Object> respMap = resp.readEntity(new GenericType<HashMap<String, Object>>() { });
		System.out.println(">> " + respMap);
		int statusCode = (Integer) respMap.get("Status");
		return statusCode;
	}

}
