package com.es.opentext.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by f.ustdag on 01.03.2019.
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.es.opentext")
public class Application
{


    public static void main(String[] args)
    { SpringApplication.run(Application.class, args);


    }



}
